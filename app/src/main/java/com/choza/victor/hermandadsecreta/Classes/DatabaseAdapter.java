package com.choza.victor.hermandadsecreta.Classes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victor on 06/12/2017.
 */

public class DatabaseAdapter {
    private static DatabaseHelper dbHelper = null;
    private Context context;

    public DatabaseAdapter(Context context) {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(context);
        }
        this.context = context;
    }

    public void addHermano(String name, String password, String status, String imageLR, String imageHR, String imageBoolean) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.NAME, name);
        cv.put(DatabaseHelper.STATUS, status);
        cv.put(DatabaseHelper.PASSWORD, password);
        cv.put(DatabaseHelper.IMAGELR, imageLR);
        cv.put(DatabaseHelper.IMAGEHR, imageHR);
        cv.put(DatabaseHelper.IMAGEBOOLEAN, imageBoolean);

        db.insert(DatabaseHelper.TABLE_NAME, null, cv);
    }

    public List<String> getList(String columnToSelect) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String column = null;
        switch (columnToSelect) {
            case "names":
                column = DatabaseHelper.NAME;
                break;
            case "status":
                column = DatabaseHelper.STATUS;
                break;
            case "imagesLR":
                column = DatabaseHelper.IMAGELR;
                break;
            case "IDs":
                column = DatabaseHelper.UID;
                break;
        }

        String[] columns = {column};
        Cursor cursor = db.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);

        List<String> list = new ArrayList<>();

        if (column != null) {
            while (cursor.moveToNext()) {
                list.add(cursor.getString(cursor.getColumnIndex(column)));
            }
            return list;
        } else {
            return null;
        }
    }

    public String getDataWithName(String columnToSelect, String name) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String column = null;
        switch (columnToSelect) {
            case "password":
                column = DatabaseHelper.PASSWORD;
                break;
            case "imageBoolean":
                column = DatabaseHelper.IMAGEBOOLEAN;
                break;
            case "imageHR":
                column = DatabaseHelper.IMAGEHR;
                break;
            case "imageLR":
                column = DatabaseHelper.IMAGELR;
                break;
        }

        if (column != null) {
            String[] columns = {column};
            Cursor cursor = db.query(DatabaseHelper.TABLE_NAME, columns, DatabaseHelper.NAME + " = '" + name + "'", null, null, null, null);
            cursor.moveToNext();

            return cursor.getString(cursor.getColumnIndex(column));
        } else {
            return null;
        }
    }

    public String getDataWithID(String columnToSelect, int id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String column = null;

        switch (columnToSelect) {
            case "imageHR":
                column = DatabaseHelper.IMAGEHR;
                break;
            case "imageLR":
                column = DatabaseHelper.IMAGELR;
                break;
            case "imageBoolean":
                column = DatabaseHelper.IMAGEBOOLEAN;
                break;
            case "name":
                column = DatabaseHelper.NAME;
                break;
            case "status":
                column = DatabaseHelper.STATUS;
                break;
            case "password":
                column = DatabaseHelper.PASSWORD;
                break;
        }

        if (column != null) {
            String[] columns = {column};
            Cursor cursor = db.query(DatabaseHelper.TABLE_NAME, columns, DatabaseHelper.UID + " = '" + id + "'", null, null, null, null);
            cursor.moveToNext();

            return cursor.getString(cursor.getColumnIndex(column));
        } else {
            return null;
        }
    }

    public String getSizeImageWithID(int id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] columnas = {DatabaseHelper.IMAGEHR};
        Cursor cursor = db.query(DatabaseHelper.TABLE_NAME, columnas, DatabaseHelper.UID + " = '" + id + "'", null, null, null, null);
        cursor.moveToNext();

        Bitmap imagen = BitmapFactory.decodeFile(cursor.getString(cursor.getColumnIndex(DatabaseHelper.IMAGEHR)));

        return imagen.getWidth() + " x " + imagen.getHeight();
    }

    public int getRowCount() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] columnas = {DatabaseHelper.IMAGELR};
        Cursor cursor = db.query(DatabaseHelper.TABLE_NAME, columnas, null, null, null, null, null);

        int numeroFilas = 0;

        while (cursor.moveToNext()) {
            numeroFilas++;
        }
        return numeroFilas;
    }

    public List<Integer> getIDs() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] columns = {DatabaseHelper.UID};
        Cursor cursor = db.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);

        List<Integer> listIDs = new ArrayList<>();

        while (cursor.moveToNext()) {
            listIDs.add(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.UID)));
        }
        return listIDs;

    }

    public void editDataWithName(String columnToSelect, String data, String name) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String column = null;

        switch (columnToSelect) {
            case "status":
                column = DatabaseHelper.STATUS;
                break;
            case "imageLR":
                column = DatabaseHelper.IMAGELR;
                break;
            case "imageHR":
                column = DatabaseHelper.IMAGEHR;
                break;
            case "imageBoolean":
                column = DatabaseHelper.IMAGEBOOLEAN;
                break;
            case "password":
                column = DatabaseHelper.PASSWORD;
                break;
        }

        if (column != null) {
            ContentValues cv = new ContentValues();
            cv.put(column, data);

            db.update(DatabaseHelper.TABLE_NAME, cv, DatabaseHelper.NAME + " = '" + name + "'", null);
        }
    }

    public void editDataWithID(String columnToSelect, String data, int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String column = null;

        switch (columnToSelect) {
            case "imageHR":
                column = DatabaseHelper.IMAGEHR;
                break;
            case "imageLR":
                column = DatabaseHelper.IMAGELR;
                break;
            case "imageBoolean":
                column = DatabaseHelper.IMAGEBOOLEAN;
                break;
            case "name":
                column = DatabaseHelper.NAME;
                break;
            case "status":
                column = DatabaseHelper.STATUS;
                break;
            case "password":
                column = DatabaseHelper.PASSWORD;
                break;
        }

        if (column != null) {
            ContentValues cv = new ContentValues();
            cv.put(column, data);

            db.update(DatabaseHelper.TABLE_NAME, cv, DatabaseHelper.UID + " = '" + id + "'", null);
        }
    }

    public void deleteUserWithName(String name) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper.NAME + " = '" + name + "'", null);
    }

    public void deleteUserWithID(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper.UID + " = '" + id + "'", null);
    }


    public class DatabaseHelper extends SQLiteOpenHelper {
        private static final String dbName = "mydatabase";
        private static final int dbVersion = 1;

        private static final String TABLE_NAME = "HERMANOS";
        private static final String UID = "ID";
        private static final String NAME = "Name";
        private static final String STATUS = "Status";
        private static final String PASSWORD = "Password";
        private static final String IMAGELR = "ImageLR";
        private static final String IMAGEHR = "ImageHR";
        private static final String IMAGEBOOLEAN = "ImageBoolean";

        public DatabaseHelper(Context context) {
            super(context, dbName, null, dbVersion);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    NAME + " VARCHAR, " + STATUS + " VARCHAR, " + PASSWORD + " VARCHAR, " + IMAGELR + " VARCHAR, " +
                    IMAGEHR + " VARCHAR, " + IMAGEBOOLEAN + " VARCHAR);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}


