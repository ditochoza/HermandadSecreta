package com.choza.victor.hermandadsecreta.Visual;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.choza.victor.hermandadsecreta.Classes.DatabaseAdapter;
import com.choza.victor.hermandadsecreta.R;

import java.util.List;

public class AdminActivity extends AppCompatActivity {
    boolean adminPasswordChanged = false;
    EditText passwordText, nameText;
    String name, password;
    DatabaseAdapter databaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        databaseAdapter = new DatabaseAdapter(this);

        passwordText = (EditText) findViewById(R.id.passwordEditTextAdmin);
        nameText = (EditText) findViewById(R.id.nameEditTextAdmin);
    }

    @Override
    public void onBackPressed() {
        if (adminPasswordChanged) { //Si se ha cambiado la contraseña, se devuelve la nueva contraseña para cambiarla en el Main
            Intent resEdit = new Intent();
            resEdit.putExtra("password", password);
            setResult(RESULT_OK, resEdit);
            finish();
        }
        super.onBackPressed();
    }

    public void addUser(View view) {
        name = nameText.getText().toString();
        password = passwordText.getText().toString();

        boolean exists = false;
        List<String> names = databaseAdapter.getList("names");
        for (int i = 0; i < names.size(); i++) {
            if (name.equals(names.get(i))) {
                exists = true;
            }
        }

        if (exists) {
            Toast.makeText(AdminActivity.this, R.string.existing_user_toast_admin, Toast.LENGTH_SHORT).show();
        } else if (name.equals("Admin") || name.equals("admin")) {
            Toast.makeText(AdminActivity.this, R.string.add_admin_toast_admin, Toast.LENGTH_SHORT).show();
        } else {
            if (!name.isEmpty() && !password.isEmpty()) {
                databaseAdapter.addHermano(name, password, "", "", "", "false");
                Toast.makeText(AdminActivity.this, R.string.added_user_toast_admin, Toast.LENGTH_SHORT).show();
                nameText.setText("");
                passwordText.setText("");

            } else if (name.isEmpty()) {
                Toast.makeText(AdminActivity.this, R.string.name_empty_toast_admin, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(AdminActivity.this, R.string.password_empty_toast_admin, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void deleteUser(View view) {
        name = nameText.getText().toString();

        boolean exists = false;
        List<String> names = databaseAdapter.getList("names");
        for (int i = 0; i < names.size(); i++) {
            if (name.equals(names.get(i))) {
                exists = true;
            }
        }

        if (exists) {
            databaseAdapter.deleteUserWithName(name);
            Toast.makeText(AdminActivity.this, R.string.user_removed_toast_admin, Toast.LENGTH_SHORT).show();
            nameText.setText("");
            passwordText.setText("");
        } else if (name.equals("Admin") || name.equals("admin")) {
            Toast.makeText(AdminActivity.this, R.string.delete_admin_toast_admin, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(AdminActivity.this, R.string.non_existing_user_toast_admin, Toast.LENGTH_SHORT).show();
        }
    }

    public void editPassword(View view) {
        name = nameText.getText().toString();
        password = passwordText.getText().toString();

        boolean exists = false;
        List<String> names = databaseAdapter.getList("names");
        for (int i = 0; i < names.size(); i++) {
            if (name.equals(names.get(i))) {
                exists = true;
            }
        }

        if (exists && !name.equals("Admin") && !name.equals("admin")) {
            if (!password.equals("")) {
                databaseAdapter.editDataWithName("password", password, name);
                Toast.makeText(AdminActivity.this, R.string.password_changed_toast_admin, Toast.LENGTH_SHORT).show();
                passwordText.setText("");
                nameText.setText("");
                nameText.setFocusable(true);
            } else {
                Toast.makeText(AdminActivity.this, R.string.password_empty_toast_admin, Toast.LENGTH_SHORT).show();
            }
        } else if (name.equals("Admin") || name.equals("admin")) {
            adminPasswordChanged = true;
            Toast.makeText(AdminActivity.this, R.string.admin_password_changed_toast_admin, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(AdminActivity.this, R.string.non_existing_user_toast_admin, Toast.LENGTH_SHORT).show();
        }
    }
}
