package com.choza.victor.hermandadsecreta.Visual;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.choza.victor.hermandadsecreta.Classes.DatabaseAdapter;
import com.choza.victor.hermandadsecreta.R;

public class ImageActivity extends AppCompatActivity {
    ImageView imageFull;
    int position;
    DatabaseAdapter databaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        databaseAdapter = new DatabaseAdapter(this);

        imageFull = (ImageView) findViewById(R.id.imageFull);

        Intent datosEntrada = getIntent();
        if (datosEntrada != null) {
            position = datosEntrada.getIntExtra("position", 0);
        }

        setImage();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setImage() {
        if (databaseAdapter.getDataWithID("imageHR", (position)).equals("")){
            imageFull.setImageDrawable(getResources().getDrawable(R.drawable.user_profile_image));
        }else{
            imageFull.setImageBitmap(BitmapFactory.decodeFile(databaseAdapter.getDataWithID("imageHR", (position))));
        }
    }
}
