package com.choza.victor.hermandadsecreta.Visual;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.choza.victor.hermandadsecreta.Classes.DatabaseAdapter;
import com.choza.victor.hermandadsecreta.Classes.Hermanos;
import com.choza.victor.hermandadsecreta.Classes.ListAdapter;
import com.choza.victor.hermandadsecreta.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    boolean admin;
    DatabaseAdapter databaseAdapter;
    ListView listView;
    ListAdapter listAdapter;
    private static final int INFO_ACTIVITY = 1;
    private static final int EDIT_ACTIVITY = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        databaseAdapter = new DatabaseAdapter(this);

        Intent datosEntrada = getIntent();
        if (datosEntrada != null) { //Se recoge si el que ha pulsado el botón ha sido el administrador o no
            admin = datosEntrada.getBooleanExtra("admin", false);
        }

        listView = (ListView) findViewById(R.id.list);

        listAdapter = new ListAdapter(createUsers(), this);

        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
    }

    //Si se pulsa un elemento de la lista, se muestra su información en otra Activity
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentInfo = new Intent(this, InfoActivity.class);
        intentInfo.putExtra("position", databaseAdapter.getIDs().get(position));

        startActivityForResult(intentInfo, INFO_ACTIVITY);
    }

    //Si se mantiene pulsado en la cuenta de administrador, se podrán editar todos los datos en otra Activity
    //También se puede eliminar al usuario
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        if (admin) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle(R.string.choose_action_alertDialog_list);
            dialog.setItems(new CharSequence[]{getString(R.string.edit_option_alertDialog_list),
                    getString(R.string.deleteText)}, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == 0) {
                        editUser(databaseAdapter.getIDs().get(position));
                    } else {
                        deleteUser(databaseAdapter.getIDs().get(position));
                    }
                }
            });
            dialog.show();
            return true;
        }else{
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(ListActivity.this, R.string.user_edited_toast_admin, Toast.LENGTH_SHORT).show();
                updateList(); //Se actualiza la lista al cambiar algo en la Activity de editar
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private List<Hermanos> createUsers() { //Se crean todos los usuarios
        ArrayList<Hermanos> users = new ArrayList<>();
        String[] imagesLR = databaseAdapter.getList("imagesLR").toArray(new String[0]);
        String[] names = databaseAdapter.getList("names").toArray(new String[0]);
        String[] status = databaseAdapter.getList("status").toArray(new String[0]);

        for (int i = 0; i < names.length; i++) {
            Hermanos newUser = new Hermanos(names[i], status[i], imagesLR[i]);
            users.add(newUser);
        }

        return users;
    }

    private void deleteUser(final int position) {
        AlertDialog.Builder dialogDelete = new AlertDialog.Builder(this);

        dialogDelete.setTitle(R.string.deleteText);
        dialogDelete.setMessage(R.string.message_delete_alertDialog_list);
        dialogDelete.setPositiveButton(R.string.positive_delete_alertDialog_list, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Se eliminan las imágenes y los datos de la base de datos
                if (databaseAdapter.getDataWithID("imageBoolean", position).equals("true")) {
                    File file1 = new File(databaseAdapter.getDataWithID("imageHR", position));
                    file1.delete();
                    File file2 = new File(databaseAdapter.getDataWithID("imageLR", position));
                    file2.delete();
                }
                databaseAdapter.deleteUserWithID(position);
                Toast.makeText(ListActivity.this, R.string.user_removed_toast_admin, Toast.LENGTH_SHORT).show();

                updateList();
            }
        });

        dialogDelete.setNegativeButton(R.string.negative_delete_alertDialog_list, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogDelete.show();
    }

    private void editUser(int position) { //Se abre la actividad para editar un usuario
        Intent intentInfo = new Intent(ListActivity.this, EditActivity.class);
        intentInfo.putExtra("position", position);

        startActivityForResult(intentInfo, EDIT_ACTIVITY);
    }

    private void updateList() { //Se vuelve a "crear" la lista
        listAdapter = new ListAdapter(createUsers(), ListActivity.this);

        listView.setAdapter(listAdapter);

        listAdapter.notifyDataSetChanged();
    }
}
