package com.choza.victor.hermandadsecreta.Classes;

/**
 * Created by Victor on 08/12/2017.
 */

public class Hermanos {
    public String name;
    public String status;
    public String thumbnailLR;

    public Hermanos(String name, String status, String thumbnailLR) {
        this.name = name;
        this.status = status;
        this.thumbnailLR = thumbnailLR;
    }
}
