package com.choza.victor.hermandadsecreta.Visual;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.choza.victor.hermandadsecreta.Classes.DatabaseAdapter;
import com.choza.victor.hermandadsecreta.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText passwordText, statusText;
    AutoCompleteTextView nameText;
    Button adminButton;
    String name, password, status;
    File imageFileHR, imageFileLR;
    DatabaseAdapter databaseAdapter;
    String[] names; //Nombres para el autocompletado
    ArrayAdapter<String> adapter;
    String adminPassword = "1234"; //Contraseña por defecto del administrador
    int permissionCheck; //Para comprobar los permisos
    private static final int PERMISOS_ACCEDER_CAMARA = 1;
    private static final int PERMISOS_ACCEDER_GALERIA = 2;
    private static final int LIST_ACTIVITY = 3;
    private static final int ADMIN_ACTIVITY = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseAdapter = new DatabaseAdapter(this);

        passwordText = (EditText) findViewById(R.id.passwordEditTextMain);
        nameText = (AutoCompleteTextView) findViewById(R.id.nameEditTextMain);
        statusText = (EditText) findViewById(R.id.statusEditTextMain);

        adminButton = (Button) findViewById(R.id.adminButtonMain);

        names = databaseAdapter.getList("names").toArray(new String[0]);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names);
        nameText.setAdapter(adapter);

        nameText.addTextChangedListener(new TextWatcher() { //Se comprueba si se introducen los datos del administrador
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if ((s.toString().equals("Admin") || s.toString().equals("admin")) && passwordText.getText().toString().equals(adminPassword)) {
                    adminButton.setVisibility(View.VISIBLE);
                    Toast.makeText(MainActivity.this, R.string.admin_mode_toast_main, Toast.LENGTH_SHORT).show();
                } else {
                    adminButton.setVisibility(View.INVISIBLE);
                }
            }
        });

        passwordText.addTextChangedListener(new TextWatcher() { //Se comprueba si se introducen los datos del administrador
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals(adminPassword) && (nameText.getText().toString().equals("admin") || nameText.getText().toString().equals("Admin"))) {
                    adminButton.setVisibility(View.VISIBLE);
                    Toast.makeText(MainActivity.this, R.string.admin_mode_toast_main, Toast.LENGTH_SHORT).show();
                } else {
                    adminButton.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISOS_ACCEDER_GALERIA:
                permissionCheck = PackageManager.PERMISSION_GRANTED;
                for(int permisson : grantResults) {
                    permissionCheck = permissionCheck + permisson;
                }
                if (grantResults.length > 0 && permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    Intent intentGaleria = new Intent(Intent.ACTION_PICK);
                    intentGaleria.setType("image/*");
                    if (intentGaleria.resolveActivity(getPackageManager()) != null) {
                        imageFileHR = null;
                        imageFileLR = null;
                        try { //Se crean las rutas de las imágenes de alta y baja resolución
                            imageFileHR = createImageFile(1);
                            imageFileLR = createImageFile(2);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (imageFileHR != null) {

                            //Se abre la Activity de galería
                            startActivityForResult(intentGaleria, PERMISOS_ACCEDER_GALERIA);
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.no_permissions_gallery_toast_main, Toast.LENGTH_SHORT).show();
                }
                break;
            case PERMISOS_ACCEDER_CAMARA:
                permissionCheck = PackageManager.PERMISSION_GRANTED;
                for(int permisson : grantResults) {
                    permissionCheck = permissionCheck + permisson;
                }

                if (grantResults.length > 0 && permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    Intent intentCamara = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intentCamara.resolveActivity(getPackageManager()) != null) {
                        imageFileHR = null;
                        imageFileLR = null;
                        try { //Se crean las rutas de las imágenes de alta y baja resolución
                            imageFileHR = createImageFile(1);
                            imageFileLR = createImageFile(2);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (imageFileHR != null) {
                            Uri cameraURI = FileProvider.getUriForFile(this,
                                    getApplicationContext().getPackageName() + ".fileprovider",
                                    imageFileHR);

                            intentCamara.putExtra(MediaStore.EXTRA_OUTPUT, cameraURI);

                            //Para versiones por debajo de Lollipop
                            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                                intentCamara.setClipData(ClipData.newRawUri("", cameraURI));
                                intentCamara.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            }

                            //Se abre la Activity de cámara
                            startActivityForResult(intentCamara, PERMISOS_ACCEDER_CAMARA);
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.no_permissions_camera_toast_main, Toast.LENGTH_SHORT).show();
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PERMISOS_ACCEDER_GALERIA:
                if (resultCode == RESULT_OK && data != null) {
                    Uri galleryURI = data.getData();

                    try {
                        //Se optimiza el tamaño de las imágenes de alta y baja resolución, y se llevan al almacenamiento
                        InputStream inputStream = getContentResolver().openInputStream(galleryURI);

                        Bitmap photo = BitmapFactory.decodeStream(inputStream);

                        /*
                        Foto en Alta Resolución
                         */
                        //Se redimensiona la imagen
                        photo = Bitmap.createScaledBitmap(photo,
                                (int) Math.round(photo.getWidth() / getResize(photo)),
                                (int) Math.round(photo.getHeight() / getResize(photo)), false);
                        ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
                        photo.compress(Bitmap.CompressFormat.JPEG, 50, bytes1); //Se comprime la imagen

                        //Se guarda la imagen
                        File f1 = new File(imageFileHR.getAbsolutePath());
                        f1.createNewFile();
                        FileOutputStream fo1 = new FileOutputStream(f1);
                        fo1.write(bytes1.toByteArray());
                        fo1.close();

                        inputStream = getContentResolver().openInputStream(galleryURI);

                        /*
                        Foto en baja resolución
                         */
                        //Se redimensiona la imagen
                        photo = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(inputStream),
                                (int) Math.round(photo.getWidth() / (getResize(photo)*2)),
                                (int) Math.round(photo.getHeight() / (getResize(photo)*2)), false);
                        ByteArrayOutputStream bytes2 = new ByteArrayOutputStream();
                        photo.compress(Bitmap.CompressFormat.JPEG, 30, bytes2); //Se comprime la imagen

                        //Se guarda la imagen
                        File f2 = new File(imageFileLR.getAbsolutePath());
                        f2.createNewFile();
                        FileOutputStream fo2 = new FileOutputStream(f2);
                        fo2.write(bytes2.toByteArray());
                        fo2.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //Si ya se habían puesto imágenes, estas se borran
                    if (databaseAdapter.getDataWithName("imageBoolean", name).equals("true")) {
                        File file1 = new File(databaseAdapter.getDataWithName("imageHR", name));
                        file1.delete();
                        File file2 = new File(databaseAdapter.getDataWithName("imageLR", name));
                        file2.delete();
                    }

                    //Se introducen en la base de datos las rutas de las dos imágenes y el boolean se pone a true
                    databaseAdapter.editDataWithName("imageLR", imageFileLR.getAbsolutePath(), name);
                    databaseAdapter.editDataWithName("imageHR", imageFileHR.getAbsolutePath(), name);
                    databaseAdapter.editDataWithName("imageBoolean", "true", name);
                }
                break;
            case PERMISOS_ACCEDER_CAMARA:
                if (resultCode == RESULT_OK) {
                    try {
                        //Se optimiza el tamaño de las imágenes de alta y baja resolución, y se llevan al almacenamiento
                        Bitmap photo = BitmapFactory.decodeFile(imageFileHR.getAbsolutePath());

                        /*
                        Foto en alta resolución
                         */
                        //Se redimensiona la imagen
                        photo = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(imageFileHR.getAbsolutePath()),
                                (int) Math.round(photo.getWidth() / getResize(photo)),
                                (int) Math.round(photo.getHeight() / getResize(photo)), false);
                        ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
                        photo.compress(Bitmap.CompressFormat.JPEG, 50, bytes1); //Se comprime la imagen

                        File f1 = new File(imageFileHR.getAbsolutePath());
                        f1.createNewFile();
                        FileOutputStream fo1 = new FileOutputStream(f1);
                        fo1.write(bytes1.toByteArray());
                        fo1.close();

                        /*
                        Foto en baja resolución
                         */
                        //Se redimensiona la imagen
                        photo = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(imageFileHR.getAbsolutePath()),
                                (int) Math.round(photo.getWidth() / (getResize(photo)*2)),
                                (int) Math.round(photo.getHeight() / (getResize(photo)*2)), false);
                        ByteArrayOutputStream bytes2 = new ByteArrayOutputStream();
                        photo.compress(Bitmap.CompressFormat.JPEG, 30, bytes2); //Se comprime la imagen

                        //Se guarda la imagen
                        File f2 = new File(imageFileLR.getAbsolutePath());
                        f2.createNewFile();
                        FileOutputStream fo2 = new FileOutputStream(f2);
                        fo2.write(bytes2.toByteArray());
                        fo2.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //Si ya se habían puesto imágenes, estas se borran
                    if (databaseAdapter.getDataWithName("imageBoolean", name).equals("true")) {
                        File file1 = new File(databaseAdapter.getDataWithName("imageHR", name));
                        file1.delete();
                        File file2 = new File(databaseAdapter.getDataWithName("imageLR", name));
                        file2.delete();
                    }

                    //Se introducen en la base de datos las rutas de las dos imágenes y el boolean se pone a true
                    databaseAdapter.editDataWithName("imageLR", imageFileLR.getAbsolutePath(), name);
                    databaseAdapter.editDataWithName("imageHR", imageFileHR.getAbsolutePath(), name);
                    databaseAdapter.editDataWithName("imageBoolean", "true", name);
                }
                break;
            case ADMIN_ACTIVITY: //Al volver de la Activity Admin, se introducen de nuevo los nombres en el Autocompletar
                names = databaseAdapter.getList("names").toArray(new String[0]);
                adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names);
                nameText.setAdapter(adapter);
                //Si se ha cambiado la contraseña, esta se cambia en esta Activity
                if (resultCode == RESULT_OK && data != null){
                    adminPassword = data.getStringExtra("password");
                    adminButton.setVisibility(View.INVISIBLE);
                }
                break;
            case LIST_ACTIVITY: //Al volver de la Activity List, se introducen de nuevo los nombres en el Autocompletar
                names = databaseAdapter.getList("names").toArray(new String[0]);
                adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names);
                nameText.setAdapter(adapter);
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showList(View view) {
        name = nameText.getText().toString();
        password = passwordText.getText().toString();
        status = statusText.getText().toString();

        boolean exists = false; //Se comprueba si el usuario existe
        List<String> names = databaseAdapter.getList("names");
        for (int i = 0; i < names.size(); i++) {
            if (name.equals(names.get(i))) {
                exists = true;
            }
        }

        if (exists) {
            if (!password.equals(databaseAdapter.getDataWithName("password", name))) {
                Toast.makeText(MainActivity.this, R.string.wrong_password_toast_main, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, R.string.correct_password_toast_main, Toast.LENGTH_SHORT).show();
                Intent intentLista = new Intent(this, ListActivity.class);
                intentLista.putExtra("admin", false);

                startActivityForResult(intentLista, LIST_ACTIVITY);
            }
        } else {
            if (adminButton.getVisibility() == View.VISIBLE) {
                Toast.makeText(MainActivity.this, R.string.access_admin_toast_main, Toast.LENGTH_SHORT).show();
                Intent intentLista = new Intent(this, ListActivity.class);
                intentLista.putExtra("admin", true);

                startActivityForResult(intentLista, LIST_ACTIVITY);

            } else {
                Toast.makeText(MainActivity.this, R.string.wrong_user_toast_main, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void editStatus(View view) {
        name = nameText.getText().toString();
        password = passwordText.getText().toString();
        status = statusText.getText().toString();

        boolean exists = false; //Se comprueba si el usuario existe
        List<String> names = databaseAdapter.getList("names");
        for (int i = 0; i < names.size(); i++) {
            if (name.equals(names.get(i))) {
                exists = true;
            }
        }

        if (exists) {
            if (!password.equals(databaseAdapter.getDataWithName("password", name))) {
                Toast.makeText(MainActivity.this, R.string.wrong_password_toast_main, Toast.LENGTH_SHORT).show();
            } else {
                databaseAdapter.editDataWithName("status", status, name);
                Toast.makeText(MainActivity.this, R.string.correct_edit_status_toast_main, Toast.LENGTH_SHORT).show();
                statusText.setText("");
            }
        } else {
            if (name.equals("admin") || name.equals("Admin")) { //Se cambia el Toast si es el administrador el que está intentando cambiar el estado
                Toast.makeText(MainActivity.this, R.string.change_status_admin_toast_main, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, R.string.wrong_user_toast_main, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void editImage(View view) {
        name = nameText.getText().toString();
        password = passwordText.getText().toString();
        status = statusText.getText().toString();

        boolean exists = false; //Se comprueba si ya existe el nombre
        List<String> names = databaseAdapter.getList("names");
        for (int i = 0; i < names.size(); i++) {
            if (name.equals(names.get(i))) {
                exists = true;
            }
        }

        if (exists) {
            if (!password.equals(databaseAdapter.getDataWithName("password", name))) { //Se comprueba si la contraseña es correcta
                Toast.makeText(MainActivity.this, R.string.wrong_password_toast_main, Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder dialog = new AlertDialog.Builder(this); //Se crea un diálogo para
                dialog.setTitle("Añadir imagen");
                dialog.setItems(new CharSequence[]{"Cámara", "Galeria"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) { //Se piden los permisos de cámara
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISOS_ACCEDER_CAMARA);
                        } else { //Se piden los permisos de galería
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISOS_ACCEDER_GALERIA);
                        }
                    }
                });
                dialog.show();
            }
        } else {
            if (name.equals("admin") || name.equals("Admin")) { //Se cambia el Toast si es el administrador el que está intentando cambiar la imagen
                Toast.makeText(MainActivity.this, R.string.change_image_admin_toast_main, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, R.string.wrong_user_toast_main, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public double getResize(Bitmap photo) { //Se redimensiona la imagen para que ocupe menos
        double resize;
        if (photo.getWidth() > photo.getHeight()) { //Horizontal
            if (photo.getHeight() <= 500) {
                resize = 1.5;
            } else if (photo.getHeight() <= 1000) {
                resize = 2;
            } else if (photo.getHeight() <= 1500) {
                resize = 2.5;
            } else {
                resize = 3;
            }
        } else { //Vertical
            if (photo.getWidth() <= 500) {
                resize = 1.5;
            } else if (photo.getWidth() <= 1000) {
                resize = 2;
            } else if (photo.getWidth() <= 1500) {
                resize = 2.5;
            } else {
                resize = 3;
            }
        }
        return resize;
    }

    private File createImageFile(int i) throws IOException { //Se crea la ruta de las imágenes
        String imageFileName;
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        if (i == 1) {
            imageFileName = "hr_image_" + timeStamp; //En alta resolución
        } else {
            imageFileName = "lr_image_" + timeStamp; //En baja resolución
        }
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    public void adminOptions(View view) { //Se abre la Activity de las opciones de administador
        Intent intentLista = new Intent(this, AdminActivity.class);

        startActivityForResult(intentLista, ADMIN_ACTIVITY);
    }
}
