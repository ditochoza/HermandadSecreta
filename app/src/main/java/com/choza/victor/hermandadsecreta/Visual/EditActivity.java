package com.choza.victor.hermandadsecreta.Visual;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.choza.victor.hermandadsecreta.Classes.DatabaseAdapter;
import com.choza.victor.hermandadsecreta.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EditActivity extends AppCompatActivity {
    String currentName;
    boolean camera = true;
    InputStream inputStream;
    Uri galleryURI;
    Bitmap photo;
    int position;
    ImageView image;
    EditText nameText, statusText, passwordText;
    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbar;
    DatabaseAdapter databaseAdapter;
    File imageFileHR, imageFileLR;
    int permissionCheck;
    private static final int PERMISOS_ACCEDER_CAMARA = 1;
    private static final int PERMISOS_ACCEDER_GALERIA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);


        toolbar = (Toolbar) findViewById(R.id.toolbarEdit);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        image = findViewById(R.id.imageViewEdit);

        nameText = findViewById(R.id.nameEditTextEdit);
        statusText = findViewById(R.id.statusEditTextEdit);
        passwordText = findViewById(R.id.passwordEditTextEdit);

        Intent datosEntrada = getIntent();
        if (datosEntrada != null) {
            position = datosEntrada.getIntExtra("position", 0);
        }

        databaseAdapter = new DatabaseAdapter(this);

        setImage();
        setData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { //Se crea el menú personalizado con el botón guardar
        getMenuInflater().inflate(R.menu.menu_actionbar_edit, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (photo != null) { //Si se ha pulsado el botón atrás y se había elegido una imagen, esta se borra
            File file1 = new File(imageFileHR.getAbsolutePath());
            file1.delete();
            File file2 = new File(imageFileLR.getAbsolutePath());
            file2.delete();
            finish();
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent resEdit;
        switch (item.getItemId()) {
            case android.R.id.home:
                if (photo != null) { //Si se ha pulsado el botón atrás y se había elegido una imagen, esta se borra
                    File file1 = new File(imageFileHR.getAbsolutePath());
                    file1.delete();
                    File file2 = new File(imageFileLR.getAbsolutePath());
                    file2.delete();
                    finish();
                }
                finish();
                return true;
            case R.id.saveActionBarEdit: //Si se pulsa el botón guardar, se introducen los datos en la base de datos

                boolean exists = false; //Se comprueba si ya existe el nombre
                List<String> names = databaseAdapter.getList("names");
                for (int i = 0; i < names.size(); i++) {
                    if (nameText.getText().toString().equals(names.get(i)) && !nameText.getText().toString().equals(currentName)) {
                        exists = true;
                    }
                }

                if (!exists) { //Si el nombre no es el de otro usuario existente se guardan todos los datos
                    databaseAdapter.editDataWithID("name", nameText.getText().toString(), position);
                    databaseAdapter.editDataWithID("status", statusText.getText().toString(), position);
                    databaseAdapter.editDataWithID("password", passwordText.getText().toString(), position);
                    saveImage(); //Se guarda la imagen seleccionada

                    resEdit = new Intent();
                    setResult(RESULT_OK, resEdit);
                    finish();
                    return true;
                } else {
                    Toast.makeText(getApplicationContext(), R.string.existing_name_toast_edit, Toast.LENGTH_SHORT).show();
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISOS_ACCEDER_GALERIA:
                permissionCheck = PackageManager.PERMISSION_GRANTED;
                for(int permisson : grantResults) {
                    permissionCheck = permissionCheck + permisson;
                }
                if (grantResults.length > 0 && permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    Intent intentGaleria = new Intent(Intent.ACTION_PICK);
                    intentGaleria.setType("image/*");

                    if (intentGaleria.resolveActivity(getPackageManager()) != null) {
                        imageFileHR = null;
                        imageFileLR = null;
                        try {
                            imageFileHR = createImageFile(1);
                            imageFileLR = createImageFile(2);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (imageFileHR != null) {
                            startActivityForResult(intentGaleria, PERMISOS_ACCEDER_GALERIA);
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.no_permissions_gallery_toast_edit, Toast.LENGTH_SHORT).show();
                }
                break;
            case PERMISOS_ACCEDER_CAMARA:
                permissionCheck = PackageManager.PERMISSION_GRANTED;
                for(int permisson : grantResults) {
                    permissionCheck = permissionCheck + permisson;
                }
                if (grantResults.length > 0 && permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    Intent intentCamara = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intentCamara.resolveActivity(getPackageManager()) != null) {// Create the File where the photo should go
                        imageFileHR = null;
                        imageFileLR = null;
                        try {
                            imageFileHR = createImageFile(1);
                            imageFileLR = createImageFile(2);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (imageFileHR != null) {
                            Uri cameraURI = FileProvider.getUriForFile(this,
                                    getApplicationContext().getPackageName() + ".fileprovider",
                                    imageFileHR);

                            intentCamara.putExtra(MediaStore.EXTRA_OUTPUT, cameraURI);

                            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                                intentCamara.setClipData(ClipData.newRawUri("", cameraURI));
                                intentCamara.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            }

                            startActivityForResult(intentCamara, PERMISOS_ACCEDER_CAMARA);
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.no_permissions_camera_toast_edit, Toast.LENGTH_SHORT).show();
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PERMISOS_ACCEDER_GALERIA:
                if (resultCode == RESULT_OK && data != null) {
                    galleryURI = data.getData();

                    try { //Se recoge la imagen y se inserta en el ImageView
                        camera = false;

                        inputStream = getContentResolver().openInputStream(galleryURI);

                        photo = BitmapFactory.decodeStream(inputStream);

                        photo = Bitmap.createScaledBitmap(photo, (int) Math.round(photo.getWidth() / getResize(photo)),
                                (int) Math.round(photo.getHeight() / getResize(photo)), false);

                        image.setImageBitmap(photo);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case PERMISOS_ACCEDER_CAMARA:
                if (resultCode == RESULT_OK) { //Se recoge la imagen y se inserta en el ImageView
                    camera = true;

                    photo = BitmapFactory.decodeFile(imageFileHR.getAbsolutePath());

                    photo = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(imageFileHR.getAbsolutePath()),
                            (int) Math.round(photo.getWidth() / getResize(photo)),
                            (int) Math.round(photo.getHeight() / getResize(photo)), false);

                    image.setImageBitmap(photo);
                }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void saveImage() {
        if (photo != null) {
            if (camera) { //Se guardan las imágenes de cámara
                try {
                    //Se optimiza el tamaño de las imágenes de alta y baja resolución, y se llevan al almacenamiento
                    Bitmap photo = BitmapFactory.decodeFile(imageFileHR.getAbsolutePath());

                    /*
                    Foto en alta resolución
                     */
                    //Se redimensiona la imagen
                    photo = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(imageFileHR.getAbsolutePath()),
                            (int) Math.round(photo.getWidth() / getResize(photo)),
                            (int) Math.round(photo.getHeight() / getResize(photo)), false);
                    ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 50, bytes1); //Se comprime la imagen

                    File f1 = new File(imageFileHR.getAbsolutePath());
                    f1.createNewFile();
                    FileOutputStream fo1 = new FileOutputStream(f1);
                    fo1.write(bytes1.toByteArray());
                    fo1.close();

                    /*
                    Foto en baja resolución
                     */
                    //Se redimensiona la imagen
                    photo = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(imageFileHR.getAbsolutePath()),
                            (int) Math.round(photo.getWidth() / (getResize(photo)*2)),
                            (int) Math.round(photo.getHeight() / (getResize(photo)*2)), false);
                    ByteArrayOutputStream bytes2 = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 30, bytes2); //Se comprime la imagen

                    //Se guarda la imagen
                    File f2 = new File(imageFileLR.getAbsolutePath());
                    f2.createNewFile();
                    FileOutputStream fo2 = new FileOutputStream(f2);
                    fo2.write(bytes2.toByteArray());
                    fo2.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //Si ya se habían puesto imágenes, estas se borran
                if (databaseAdapter.getDataWithID("imageBoolean", position).equals("true")) {
                    File file1 = new File(databaseAdapter.getDataWithID("imageHR", position));
                    file1.delete();
                    File file2 = new File(databaseAdapter.getDataWithID("imageLR", position));
                    file2.delete();
                }

                //Se introducen en la base de datos las rutas de las dos imágenes y el boolean se pone a true
                databaseAdapter.editDataWithID("imageLR", imageFileLR.getAbsolutePath(), position);
                databaseAdapter.editDataWithID("imageHR", imageFileHR.getAbsolutePath(), position);
                databaseAdapter.editDataWithID("imageBoolean", "true", position);
            } else {
                try {
                    //Se optimiza el tamaño de las imágenes de alta y baja resolución, y se llevan al almacenamiento
                    InputStream inputStream = getContentResolver().openInputStream(galleryURI);

                    Bitmap photo = BitmapFactory.decodeStream(inputStream);

                    /*
                    Foto en Alta Resolución
                     */
                    //Se redimensiona la imagen
                    photo = Bitmap.createScaledBitmap(photo,
                            (int) Math.round(photo.getWidth() / getResize(photo)),
                            (int) Math.round(photo.getHeight() / getResize(photo)), false);
                    ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 50, bytes1); //Se comprime la imagen

                    //Se guarda la imagen
                    File f1 = new File(imageFileHR.getAbsolutePath());
                    f1.createNewFile();
                    FileOutputStream fo1 = new FileOutputStream(f1);
                    fo1.write(bytes1.toByteArray());
                    fo1.close();

                    inputStream = getContentResolver().openInputStream(galleryURI);

                    /*
                    Foto en baja resolución
                     */
                    //Se redimensiona la imagen
                    photo = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(inputStream),
                            (int) Math.round(photo.getWidth() / (getResize(photo)*2)),
                            (int) Math.round(photo.getHeight() / (getResize(photo)*2)), false);
                    ByteArrayOutputStream bytes2 = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 30, bytes2); //Se comprime la imagen

                    //Se guarda la imagen
                    File f2 = new File(imageFileLR.getAbsolutePath());
                    f2.createNewFile();
                    FileOutputStream fo2 = new FileOutputStream(f2);
                    fo2.write(bytes2.toByteArray());
                    fo2.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //Si ya se habían puesto imágenes, estas se borran
                if (databaseAdapter.getDataWithID("imageBoolean", position).equals("true")) {
                    File file1 = new File(databaseAdapter.getDataWithID("imageHR", position));
                    file1.delete();
                    File file2 = new File(databaseAdapter.getDataWithID("imageLR", position));
                    file2.delete();
                }

                //Se introducen en la base de datos las rutas de las dos imágenes y el boolean se pone a true
                databaseAdapter.editDataWithID("imageLR", imageFileLR.getAbsolutePath(), position);
                databaseAdapter.editDataWithID("imageHR", imageFileHR.getAbsolutePath(), position);
                databaseAdapter.editDataWithID("imageBoolean", "true", position);
            }
        }
    }

    private void setImage() { //Se pone la imagen de la base de datos
        if (databaseAdapter.getDataWithID("imageHR", (position)).equals("")) {
            image.setImageDrawable(getResources().getDrawable(R.drawable.user_profile_image));
        } else {
            image.setImageBitmap(BitmapFactory.decodeFile(databaseAdapter.getDataWithID("imageHR", position)));
        }
    }

    private void setData() { //Se ponen los datos de la base de datos
        currentName = databaseAdapter.getDataWithID("name", position);
        nameText.setText(databaseAdapter.getDataWithID("name", position));
        statusText.setText(databaseAdapter.getDataWithID("status", position));
        passwordText.setText(databaseAdapter.getDataWithID("password", position));

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayoutEdit);
        collapsingToolbar.setTitle(databaseAdapter.getDataWithID("name", position));
    }

    public void editImage(View view) { //Cuadro de diálogo con opción de cámara y galería
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Añadir imagen");
        dialog.setItems(new CharSequence[]{"Cámara", "Galeria"}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    ActivityCompat.requestPermissions(EditActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PERMISOS_ACCEDER_CAMARA);
                } else {
                    ActivityCompat.requestPermissions(EditActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISOS_ACCEDER_GALERIA);
                }
            }
        });
        dialog.show();
    }

    public double getResize(Bitmap photo) {
        double resize;
        if (photo.getWidth() > photo.getHeight()) { //Horizontal
            if (photo.getHeight() <= 500) {
                resize = 1.5;
            } else if (photo.getHeight() <= 1000) {
                resize = 2;
            } else if (photo.getHeight() <= 1500) {
                resize = 2.5;
            } else {
                resize = 3;
            }
        } else { //Vertical
            if (photo.getWidth() <= 500) {
                resize = 1.5;
            } else if (photo.getWidth() <= 1000) {
                resize = 2;
            } else if (photo.getWidth() <= 1500) {
                resize = 2.5;
            } else {
                resize = 3;
            }
        }
        return resize;
    }

    private File createImageFile(int i) throws IOException {
        String imageFileName;
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        if (i == 1) {
            imageFileName = "hr_image_" + timeStamp;
        } else {
            imageFileName = "lr_image_" + timeStamp;
        }
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }
}
