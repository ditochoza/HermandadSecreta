package com.choza.victor.hermandadsecreta.Classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.choza.victor.hermandadsecreta.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victor on 08/12/2017.
 */

public class ListAdapter extends BaseAdapter{
    private List<Hermanos> users;
    private List<Bitmap> images;
    private LayoutInflater inflater;
    private Context context;
    private DatabaseAdapter databaseAdapter;

    public ListAdapter(List<Hermanos> users, Context context){
        this.users = users;
        inflater = LayoutInflater.from(context);
        this.context = context;
        databaseAdapter = new DatabaseAdapter(context);

        loadImages();
    }

    private void loadImages() { //Se cargan todas las imágenes de baja resolución para no tener que cargarlas al hacer scroll
        images = new ArrayList<>();
        for (int i = 0; i < databaseAdapter.getRowCount(); i++){
            images.add(getCompressedImage(i));
        }
    }



    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View layout = convertView;
        ViewHolder viewHolder;

        if (layout == null){
            layout = inflater.inflate(R.layout.list,parent,false);
            viewHolder = new ViewHolder(layout);
            layout.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) layout.getTag();
        }

        viewHolder.nameTextView.setText(users.get(position).name);
        viewHolder.statusTextView.setText(users.get(position).status);
        viewHolder.thumbnailImageView.setImageBitmap(images.get(position));


        return layout;
    }

    private Bitmap getCompressedImage(int i) { //Se obtiene la imagen de baja resolución
        Bitmap image;
        if (users.get(i).thumbnailLR.equals("")){
            image = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.user_profile_image);
        }else{
            image = BitmapFactory.decodeFile(users.get(i).thumbnailLR);
        }
        return image;
    }
}
