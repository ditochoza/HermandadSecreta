package com.choza.victor.hermandadsecreta.Classes;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.choza.victor.hermandadsecreta.R;

/**
 * Created by Victor on 08/12/2017.
 */

public class ViewHolder {
    public TextView nameTextView;
    public TextView statusTextView;
    public ImageView thumbnailImageView;

    ViewHolder(View view){
        nameTextView = view.findViewById(R.id.title);
        statusTextView = view.findViewById(R.id.subtitle);
        thumbnailImageView = view.findViewById(R.id.image);
    }
}
