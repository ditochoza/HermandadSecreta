package com.choza.victor.hermandadsecreta.Visual;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.choza.victor.hermandadsecreta.Classes.DatabaseAdapter;
import com.choza.victor.hermandadsecreta.R;

public class InfoActivity extends AppCompatActivity {
    int position;
    ImageView imageView;
    TextView statusText, imageSizeText;
    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbar;
    DatabaseAdapter databaseAdapter;
    private static final int IMAGE_ACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);


        toolbar = (Toolbar) findViewById(R.id.toolbarInfo);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        imageView = findViewById(R.id.imageViewInfo);

        statusText = findViewById(R.id.statusTextViewEdit);
        imageSizeText = findViewById(R.id.imageSizeTextViewInfo);

        Intent intentData = getIntent();
        if (intentData != null) {
            position = intentData.getIntExtra("position", 0);
        }

        databaseAdapter = new DatabaseAdapter(this);

        setImage();
        setData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home: //Si se pulsa el botón atrás del Action Bar, se cierra la Actividad
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setData() { //Se ponen los datos de información
        if (databaseAdapter.getDataWithID("imageHR", (position)).equals("")) {
            imageSizeText.setText("256 x 256");
        } else {
            imageSizeText.setText(databaseAdapter.getSizeImageWithID(position));
        }

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayoutInfo);
        collapsingToolbar.setTitle(databaseAdapter.getDataWithID("name", position));

        statusText.setText(databaseAdapter.getDataWithID("status", position));
    }

    private void setImage() { //Se pone la imagen en alta resolución
        if (databaseAdapter.getDataWithID("imageHR", position).equals("")) {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.user_profile_image));
        } else {
            imageView.setImageBitmap(BitmapFactory.decodeFile(databaseAdapter.getDataWithID("imageHR", position)));
        }
    }

    public void openImage(View view) { //Se abre la Actividad con la imagen en pantalla completa
        Intent intentImage = new Intent(this, ImageActivity.class);
        Bundle bundle = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            bundle = ActivityOptions.makeSceneTransitionAnimation(this, (ImageView) findViewById(R.id.imageViewInfo),
                    findViewById(R.id.imageViewInfo).getTransitionName()).toBundle();
        }
        intentImage.putExtra("position", position);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            startActivity(intentImage, bundle);
        }else{
            startActivityForResult(intentImage, IMAGE_ACTIVITY);
        }
    }
}
